<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(User::class, 10)->create();

        factory(App\User::class, 3)->create()->each(function ($user) {
            $user->contact_list()->saveMany(factory(App\Contact::class, 5)->make());
        });
        
    }
}
]